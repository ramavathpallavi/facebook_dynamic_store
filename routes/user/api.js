const router = require('express').Router();
const randomstring = require("randomstring");
const config = require('../../config/config');
const Storename = require('../../models/storename');
const passport=require('../../config/passport');

/*	router.get('/', (req, res) => {

		
		console.log(req.get('host'));
		const url=req.get('host');
		const store=url.split(".");
		const storeName=store[0];
		console.log(store[0]);
		Storename.selectTable('store_details');
		Storename.getItem(storeName, {}, (err, name) => {
			console.log(name, err);
			if (err) {
			} if (Object.keys(name).length === 0) {
				console.log("storename not found");
				res.render("err");
			}
			if (Object.keys(name).length > 0) {
				res.render("login");
			}
		});
	
		
	})


	router.get("/login",function(req,res){
		res.render("login");
	   
 })
	

*/	   

router.get("/fb",function(req,res){
	  res.redirect('/fb/auth/facebook');
})

router.get("/fb/logincheck",isLoggedIn,function(req,res){
	  var json=JSON.stringify(req.user);
	  console.log("login user: "+json);
          console.log(req.get('host'));
          const url=req.get('host');
          var username=req.user.username;
          console.log("username::"+username);
          const queryParams =JSON.stringify(req.query);

          const store=url.split(".");
          const storeName=store[0];
          console.log("storename:"+store[0]);
          Storename.selectTable('store_details in login');
          res.redirect("https://"+storeName+"."+"cadenza.tech/?username="+username);
   
//	res.redirect("https://www.google.com");
})

router.get('/fb/auth/storeName', (req, res) => {
	if(req.query.storename){
		const storeName = req.query.storename;
		req.session.storename = storeName;
		res.redirect("http://"+storeName+".cadenza.tech/fb/auth/facebook");
	}
})

router.get('/fb/auth/facebook', passport.authenticate('facebook', { scope : ['public_profile', 'email'] }));

router.get('/fb/auth/facebook/callback',passport.authenticate('facebook', {
                    successRedirect : '/fb/logincheck',
                    failureRedirect : '/fb'
}));

router.get('/fb/logout', function(req, res) {
	req.logout();
	res.redirect('/fb/logincheck');
});

function isLoggedIn(req, res, next) {
	if (req.isAuthenticated()) {
	  return next();
	}
	
	// const url=req.get('host');
        //  const store=url.split(".");
        //  const storeName=store[0];

       // res.redirect("https://"+storeName+"."+"cadenza.tech");
	
	

	res.redirect('/fb');
  }

module.exports = router;
